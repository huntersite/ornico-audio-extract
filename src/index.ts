#!/usr/bin/env node

import chalk from 'chalk';
import clear from 'clear';
import figlet from 'figlet';
import path from 'path';

clear();
console.log(
  chalk.red(
    figlet.textSync('extract-cli', { horizontalLayout: 'full' })
  )
);

const reader = require("readline-sync");

var filename = reader.questionPath("Location of the video file: ", {
  isFile: true
});
var startTime = reader.questionInt("How many seconds in the video file do you wish to start (s): ")
var endTime = reader.questionInt("How many seconds in the video file do you wish to end (s): ")
var outputFile = reader.questionPath("Location of the extracted audio file: ", {
  isFile: true
});

var full_path = path.resolve(filename);
console.log(full_path);

const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const ffmpeg = require('fluent-ffmpeg');
ffmpeg.setFfmpegPath(ffmpegPath);

var command = ffmpeg(full_path);

command.inputOptions([
    `-ss ${startTime}`,
    `-to ${endTime}`
]);

command.outputOptions([
  '-vn',
  '-acodec copy',
]).save(outputFile);